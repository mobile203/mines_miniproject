import 'dart:io';
import 'Game.dart';
import 'Mines.dart';

class Board {
  var side;
  var board;
  var minesX;
  var minesY;
  var symbol = 0;
  var count = 0;

  Mines mines = Mines();

  Board(int side) {
    this.side = side;
  }

  void setSide(int side) {
    this.side = side;
  }

  void createBoard(maxMines) {
    board = List.generate(side, (i) => List.generate(side, (j) => "-"));
    mines.setMaxMines(maxMines);
    mines.randomMines(side);
    minesX = mines.getXMines();
    minesY = mines.getYMines();
    print(minesY);
    print(minesX);
  }

  printBoard() {
    stdout.write(" ");
    for (int i = 0; i < side; i++) {
      stdout.write(" $i");
    }
    print("");
    for (int r = 0; r < side; r++) {
      for (int c = 0; c < side; c++) {
        if (c == 0) {
          stdout.write(r);
        }
        stdout.write(" ${board[r][c]}");
      }
      print("");
    }
  }

  void updateBoard(var x_row, var y_col) {
    isWin();
    stdout.write(" ");
    for (int i = 0; i < side; i++) {
      stdout.write(" $i");
    }
    print("");
    for (int r = 0; r < side; r++) {
      for (int c = 0; c < side; c++) {
        if (c == 0) {
          stdout.write(r);
        }
        stdout.write(" ${board[r][c]}");
      }
      print("");
    }
  }

  bool checkMines(int row, int col) {
    if (board[row][col] != "-") {
      return false;
    }
    count++;
    symbol = 0;
    if (row < 0 || row > side || col < 0 || col > side) {
      print("Can't input");
      return false;
    }

    for (int i = 0; i < mines.getMaxMines(); i++) {
      if (row == minesY[i] && col == minesX[i]) {
        for (int i = 0; i < mines.getMaxMines(); i++) {
          board[minesY[i]][minesX[i]] = "x";
        }
        printBoard();
        print("boomm!");
        print("Game Over. YOU LOSE!");
        exit(0);
      }
      if (row == 0) {
        if (col == 0) {
          if (col + 1 == minesX[i] && minesY[i] == row) {
            symbol++;
          } else if (row + 1 == minesY[i] && minesX[i] == col) {
            symbol++;
          } else if (col + 1 == minesX[i] && row + 1 == minesY[i]) {
            symbol++;
          }
        } else if (col == side) {
          if (col - 1 == minesX[i] && minesY[i] == row) {
            symbol++;
          } else if (row + 1 == minesY[i] && minesX[i] == col) {
            symbol++;
          } else if (col - 1 == minesX[i] && row + 1 == minesY[i]) {
            symbol++;
          }
        } else {
          if (col - 1 == minesX[i] && minesY[i] == row) {
            symbol++;
          } else if (col + 1 == minesX[i] && minesY[i] == row) {
            symbol++;
          } else if (row + 1 == minesY[i] && minesX[i] == col) {
            symbol++;
          } else if (col + 1 == minesX[i] && row + 1 == minesY[i]) {
            symbol++;
          } else if (col - 1 == minesX[i] && row + 1 == minesY[i]) {
            symbol++;
          }
        }
      } else if (row == side) {
        if (col == 0) {
          if (row - 1 == minesY[i] && col == minesX[i]) {
            symbol++;
          } else if (row - 1 == minesY[i] && col + 1 == minesX[i]) {
            symbol++;
          } else if (col + 1 == minesX[i] && row == minesY[i]) {
            symbol++;
          }
        } else if (col == side) {
          if (row - 1 == minesY[i] && col == minesX[i]) {
            symbol++;
          } else if (row - 1 == minesY[i] && col - 1 == minesX[i]) {
            symbol++;
          } else if (col - 1 == minesX[i] && row == minesY[i]) {
            symbol++;
          }
        } else {
          if (col - 1 == minesX[i] && row == minesY[i]) {
            symbol++;
          } else if (col - 1 == minesX[i] && row - 1 == minesY[i]) {
            symbol++;
          } else if (row - 1 == minesY[i] && col == minesX[i]) {
            symbol++;
          } else if (row - 1 == minesY[i] && col + 1 == minesX[i]) {
            symbol++;
          } else if (col + 1 == minesX[i] && row == minesY[i]) {
            symbol++;
          }
        }
      } else if (col == 0) {
        if (row - 1 == minesY[i] && col == minesX[i]) {
          symbol++;
        } else if (row - 1 == minesY[i] && col + 1 == minesX[i]) {
          symbol++;
        } else if (col + 1 == minesX[i] && row == minesY[i]) {
          symbol++;
        } else if (col + 1 == minesX[i] && row + 1 == minesY[i]) {
          symbol++;
        } else if (row + 1 == minesY[i] && col == minesX[i]) {
          symbol++;
        }
      } else if (col == side) {
        if (row - 1 == minesY[i] && col == minesX[i]) {
          symbol++;
        } else if (row - 1 == minesY[i] && col - 1 == minesX[i]) {
          symbol++;
        } else if (col - 1 == minesX[i] && row == minesY[i]) {
          symbol++;
        } else if (col - 1 == minesX[i] && row + 1 == minesY[i]) {
          symbol++;
        } else if (row + 1 == minesY[i] && col == minesX[i]) {
          symbol++;
        }
      } else {
        if (row - 1 == minesY[i] && col == minesX[i]) {
          symbol++;
        } else if (row - 1 == minesY[i] && col + 1 == minesX[i]) {
          symbol++;
        } else if (row - 1 == minesY[i] && col - 1 == minesX[i]) {
          symbol++;
        } else if (col - 1 == minesX[i] && row == minesY[i]) {
          symbol++;
        } else if (col - 1 == minesX[i] && row + 1 == minesY[i]) {
          symbol++;
        } else if (col + 1 == minesX[i] && row == minesY[i]) {
          symbol++;
        } else if (col + 1 == minesX[i] && row + 1 == minesY[i]) {
          symbol++;
        } else if (row + 1 == minesY[i] && col == minesX[i]) {
          symbol++;
        }
      }
    }
    board[row][col] = '$symbol';
    updateBoard(row, col);
    return true;
  }

  void isWin() {
    if (count == (side * side) - mines.getMaxMines()) {
      print("You win");
      for (int i = 0; i < mines.getMaxMines(); i++) {
        board[minesY[i]][minesX[i]] = "x";
      }
      printBoard();
      exit(0);
    }
  }
}
